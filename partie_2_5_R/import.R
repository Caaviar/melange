#load data

# jaffe = read.csv2("dataset/jaffe.csv", sep = ",")
# minst5 = read.csv2("dataset/mnist.csv", sep = ",")
# mfea = read.csv2("dataset/mfeat.csv", sep = ",")
# usps = read.csv2("dataset/usps.csv", sep = ",")
# optidigits = read.csv2("dataset/optdigits.csv", sep = ",")



jaffe_<-readMat("dataset/jaffe.mat")
jaffe<-data.frame(jaffe_$X)
y_jaffe<-data.frame(jaffe_$y)

mfeat1_<-readMat("dataset/MFEAT1.mat")
mfeat<-data.frame(mfeat1_$X)
y_mfeat<-data.frame(mfeat1_$y)

mnist5_<-readMat("dataset/MNIST5.mat")
mnist5<-data.frame(mnist5_$X)
y_mnist5<-data.frame(mnist5_$y)

optdigits_<-readMat("dataset/Optdigits.mat")
optdigits<-data.frame(optdigits_$X)
y_optdigits<-data.frame(optdigits_$y)

USPS_<-readMat("dataset/USPS.mat")
usps<-data.frame(USPS_$X)
y_usps<-data.frame(USPS_$y)



re0_ = readMat("dataset/Cluto/re0.mat")
re0<- as.matrix(re0_$mat)
y_re0<-data.frame(re0_$labels)

tr23_ = readMat("dataset/Cluto/tr23.mat")
tr23<- as.matrix(tr23_$mat)
y_tr23<-data.frame(tr23_$labels)

cacmcisi_ = readMat("dataset/Cluto/cacmcisi.mat")
cacmcisi<-as.matrix(cacmcisi_$mat)
y_cacmcisi<-data.frame(cacmcisi_$labels)

wap_ = readMat("dataset/Cluto/wap.mat")
wap<-as.matrix(wap_$mat)
y_wap<-data.frame(wap_$label.names)


classic_=readMat("dataset/cluto/classic.mat")
